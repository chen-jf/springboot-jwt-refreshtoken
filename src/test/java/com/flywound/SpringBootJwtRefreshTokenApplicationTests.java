package com.flywound;

import com.flywound.mapper.SysUserRoleMapper;
import com.flywound.service.PermissionService;
import com.flywound.service.RedisService;
import com.flywound.service.RoleService;
import com.flywound.utils.JwtTokenUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBootJwtRefreshTokenApplicationTests {

    @Autowired
    private RedisService redisService;
    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;
    @Autowired
    private RoleService roleService;
    @Autowired
    private PermissionService permissionService;
    @Test
    public void contextLoads() {
        JwtTokenUtil.validateToken("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.qOgSMwV6D_qu24SuQ2Q1eUqF439rb9xkzAjZz8Kzf5U");
    }

}
