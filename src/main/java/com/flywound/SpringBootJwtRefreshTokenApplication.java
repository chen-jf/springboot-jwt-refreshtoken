package com.flywound;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.flywound.mapper")
public class SpringBootJwtRefreshTokenApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootJwtRefreshTokenApplication.class, args);
    }

}
