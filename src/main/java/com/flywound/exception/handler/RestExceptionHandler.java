package com.flywound.exception.handler;

import com.flywound.exception.BusinessException;
import com.flywound.exception.code.BaseRespMsgEnum;
import com.flywound.utils.DataResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.AuthorizationException;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestControllerAdvice
@Slf4j
public class RestExceptionHandler {

    /**
     * 系统繁忙，请稍候再试
     *
     * @param e
     * @return DataResult<T>
     * @Version: 0.0.1
     */
    @ExceptionHandler(Exception.class)
    public <T> DataResult<T> handleException(Exception e) {
        log.error("Exception,exception:{}", e);
        return DataResult.getResult(BaseRespMsgEnum.SYSTEM_BUSY);
    }

    /**
     * 自定义全局异常处理
     *
     * @param e
     * @return DataResult<T>
     * @Version: 0.0.1
     */
    @ExceptionHandler(value = BusinessException.class)
    public <T> DataResult<T> businessExceptionHandler(BusinessException e) {
        log.error("BusinessException,exception:{}", e);
        return new DataResult<>(e.getMessageCode(), e.getDetailMessage());
    }

    /**
     * 没有权限 返回403视图
     *
     * @param
     * @return org.springframework.web.servlet.ModelAndView
     * @Version: 0.0.1
     */
    @ExceptionHandler(value = AuthorizationException.class)
    public <T> DataResult<T> errorPermission(AuthorizationException e) {
        log.error("BusinessException,exception:{}", e);
        return new DataResult<>(BaseRespMsgEnum.UNAUTHORIZED_ERROR);

    }

    /**
     * 处理validation 框架异常
     *
     * @param e
     * @return com.hth.cloud.common.base.HgResult<T>
     * @Version: 0.0.1
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    <T> DataResult<T> methodArgumentNotValidExceptionHandler(MethodArgumentNotValidException e) {
        log.error("methodArgumentNotValidExceptionHandler bindingResult.allErrors():{},exception:{}", e.getBindingResult().getAllErrors(), e);
        List<ObjectError> errors = e.getBindingResult().getAllErrors();
        return createValidExceptionResp(errors);
    }

    private <T> DataResult<T> createValidExceptionResp(List<ObjectError> errors) {
        String[] msgs = new String[errors.size()];
        int i = 0;
        for (ObjectError error : errors) {
            msgs[i] = error.getDefaultMessage();
            log.info("msg={}", msgs[i]);
            i++;
        }
        return DataResult.getResult(BaseRespMsgEnum.METHODARGUMENTNOTVALIDEXCEPTION.getCode(), msgs[0]);
    }

}
