package com.flywound.exception.code;

public interface ResponseCodeInterface {
    int getCode();
    String getMsg();
}
