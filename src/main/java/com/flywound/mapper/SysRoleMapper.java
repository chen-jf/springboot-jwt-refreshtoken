package com.flywound.mapper;

import com.flywound.entity.SysRole;
import com.flywound.vo.req.RolePageReqVO;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface SysRoleMapper {
    int deleteByPrimaryKey(String id);

    int insert(SysRole record);

    int insertSelective(SysRole record);

    SysRole selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SysRole record);

    int updateByPrimaryKey(SysRole record);

    List<SysRole> selectAll(RolePageReqVO vo);

    List<SysRole> getRoleInfoByIds(List<String> ids);


}