package com.flywound.mapper;

import com.flywound.entity.SysLog;
import com.flywound.vo.req.SysLogPageReqVO;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface SysLogMapper {
    int deleteByPrimaryKey(String id);

    int insert(SysLog record);

    int insertSelective(SysLog record);

    SysLog selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SysLog record);

    int updateByPrimaryKey(SysLog record);

    List<SysLog> selectAll(SysLogPageReqVO vo);

    void batchDeletedLog(List<String> logIds);
}