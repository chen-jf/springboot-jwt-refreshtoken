package com.flywound.mapper;

import com.flywound.entity.SysPermission;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface SysPermissionMapper {
    int deleteByPrimaryKey(String id);

    int insert(SysPermission record);

    int insertSelective(SysPermission record);

    SysPermission selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SysPermission record);

    int updateByPrimaryKey(SysPermission record);

    List<SysPermission> selectInfoByIds(List<String> ids);

    List<SysPermission> selectAll();

    List<SysPermission> selectChild(String pid);
}