package com.flywound.service;

import com.flywound.entity.SysPermission;
import com.flywound.vo.req.PermissionAddReqVO;
import com.flywound.vo.req.PermissionPageReqVO;
import com.flywound.vo.req.PermissionUpdateReqVO;
import com.flywound.vo.resp.PageVO;
import com.flywound.vo.resp.PermissionRespNode;

import java.util.List;
import java.util.Set;

public interface PermissionService {

    List<SysPermission> getPermission(String userId);

    SysPermission addPermission(PermissionAddReqVO vo);

    SysPermission detailInfo(String permissionId);

    void updatePermission(PermissionUpdateReqVO vo);

    void deleted(String permissionId);

    PageVO<SysPermission> pageInfo(PermissionPageReqVO vo);

    List<SysPermission> selectAll();

    Set<String> getPermissionsByUserId(String userId);

    List<PermissionRespNode> permissionTreeList(String userId);

    List<PermissionRespNode> selectAllByTree();

    List<PermissionRespNode> selectAllMenuByTree(String permissionId);

}
