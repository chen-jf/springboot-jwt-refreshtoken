package com.flywound.service;

import com.flywound.entity.SysLog;
import com.flywound.vo.req.SysLogPageReqVO;
import com.flywound.vo.resp.PageVO;

import java.util.List;

public interface LogService {

    PageVO<SysLog> pageInfo(SysLogPageReqVO vo);

    void deleted(List<String> logIds);
}
