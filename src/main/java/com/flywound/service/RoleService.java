package com.flywound.service;

import com.flywound.entity.SysRole;
import com.flywound.vo.req.RoleAddReqVO;
import com.flywound.vo.req.RolePageReqVO;
import com.flywound.vo.req.RoleUpdateReqVO;
import com.flywound.vo.resp.PageVO;

import java.util.List;

public interface RoleService {

    SysRole addRole(RoleAddReqVO vo);

    void updateRole(RoleUpdateReqVO vo, String accessToken);

    SysRole detailInfo(String id);

    void deletedRole(String id);

    PageVO<SysRole> pageInfo(RolePageReqVO vo);

    List<SysRole> getRoleInfoByUserId(String userId);

    List<String> getRoleNames(String userId);

    List<SysRole> selectAllRoles();
}
