package com.flywound.service;

import com.flywound.vo.resp.HomeRespVO;

public interface HomeService {

    HomeRespVO getHomeInfo(String userId);
}
