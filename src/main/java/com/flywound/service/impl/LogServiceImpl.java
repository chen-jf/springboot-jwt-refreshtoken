package com.flywound.service.impl;

import com.github.pagehelper.PageHelper;
import com.flywound.entity.SysLog;
import com.flywound.mapper.SysLogMapper;
import com.flywound.service.LogService;
import com.flywound.utils.PageUtils;
import com.flywound.vo.req.SysLogPageReqVO;
import com.flywound.vo.resp.PageVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LogServiceImpl implements LogService {
    @Autowired
    private SysLogMapper sysLogMapper;

    @Override
    public PageVO<SysLog> pageInfo(SysLogPageReqVO vo) {

        PageHelper.startPage(vo.getPageNum(), vo.getPageSize());
        List<SysLog> sysLogs = sysLogMapper.selectAll(vo);
        return PageUtils.getPageVO(sysLogs);
    }

    @Override
    public void deleted(List<String> logIds) {
        sysLogMapper.batchDeletedLog(logIds);
    }
}
