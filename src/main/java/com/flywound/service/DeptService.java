package com.flywound.service;

import com.flywound.entity.SysDept;
import com.flywound.entity.SysUser;
import com.flywound.vo.req.DeptAddReqVO;
import com.flywound.vo.req.DeptPageReqVO;
import com.flywound.vo.req.DeptUpdateReqVO;
import com.flywound.vo.req.UserPageUserByDeptReqVO;
import com.flywound.vo.resp.DeptRespNodeVO;
import com.flywound.vo.resp.PageVO;

import java.util.List;

public interface DeptService {

    SysDept addDept(DeptAddReqVO vo);

    void updateDept(DeptUpdateReqVO vo);

    SysDept detailInfo(String id);

    void deleted(String id);

    PageVO<SysDept> pageInfo(DeptPageReqVO vo);


    List<DeptRespNodeVO> deptTreeList(String deptId);

    PageVO<SysUser> pageDeptUserInfo(UserPageUserByDeptReqVO vo);

    List<SysDept> selectAll();
}
